package ntnu.idatt2001.henriabu.oblig3;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class PokerTest {
    @Nested
    class Testing {
        DeckOfCards deckOfCards = new DeckOfCards();
        PokerHand pokerHand;

        @Test
        @DisplayName("Size of pokerhand should be the same as the user chose")
        public void sizeOfPokerHandShouldBe5() {
            pokerHand = new PokerHand(5, deckOfCards);
            Assertions.assertEquals(5, pokerHand.getCardHand().size());
        }

        @Test
        @DisplayName("To many cards dealt. IllegalArgumentException should be thrown")
        public void ToManyCardsDealt() throws IllegalArgumentException {
            Assertions.assertThrows(IllegalArgumentException.class, () -> {
                pokerHand = new PokerHand(10, deckOfCards);
            });
        }

    }
}
