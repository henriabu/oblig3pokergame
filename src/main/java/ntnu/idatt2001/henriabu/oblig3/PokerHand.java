package ntnu.idatt2001.henriabu.oblig3;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Class representing a poker hand
 */

public class PokerHand {
    private ArrayList<PlayingCard> cardHand;

    /**
     * Takes an int and a deckOfCards as inputs. Sets the Arraylist cardHand equal to
     * the Arraylist created when the DeckOfCards' dealHand method is called.
     * @param numberOfCards Decides the size of the hand dealt
     * @param deckOfCards
     */

    public PokerHand(int numberOfCards, DeckOfCards deckOfCards){
        this.cardHand = deckOfCards.dealHand(numberOfCards);
    }

    /**
     * Checks if there is at least 5 cards of the specified suit.
     * @param a the suit which is checked fpr
     * @return Returns true if there is at least 5 of the suit. The hand will therefore have a flush
     */
    public boolean checkColorForFlush(char a){
        if(cardHand.stream().filter(p -> p.getSuit() == a).collect(Collectors.toList()).size()>=5){
            return true;
        }
        return false;
    }

    /**
     * Checks if the hand has a flush
     * @return  Returns true if checkColorForFlush is true one of the suits
     */
    public boolean checkFlush(){
        if (checkColorForFlush('C') || checkColorForFlush('H') || checkColorForFlush('S') || checkColorForFlush('D')){
            return true;
        }
        return false;
    }

    /**
     *
     * @return Returns the card hand
     */
    public ArrayList<PlayingCard> getCardHand(){
        return cardHand;
    }

    /**
     * Calculates the sum of the faces in the poker hand
     * @return the sum as an int
     */
    public int calcutlateSumOfFaces(){
        return cardHand.stream().map(PlayingCard::getFace).reduce((a,b) -> a+b).get();
    }

    /**
     * Iterates through the cardhand adding the cards with suit hearts'
     * "getAsString" methods to a string.
     * @return the String of hearts if there is at least one card with the suit hearts.
     * Returns "Hand has no hears" if there is no cards with suit heart.
     */
    public String onlyHearts(){
        StringBuilder text = new StringBuilder();
        if ((cardHand.stream().filter(e -> e.getSuit() == 'H').collect(Collectors.toList())).size()>0){
            text.append("Hearts on hand: \n");
            cardHand.stream().filter(e-> e.getSuit() == 'H').collect(Collectors.toList()).forEach(e -> text.append(e.getAsString() + " ") );
        }
        else{text.append("Hand has no hearts.");}
        return text.toString();
    }

    /**
     *
     * @return Returns true if the hand contains queen of spades.
     */
    public boolean checkForQueenOfSpades(){
        return cardHand.stream().anyMatch(e -> e.getSuit() == 'S' && e.getFace() == 12);
    }
}
