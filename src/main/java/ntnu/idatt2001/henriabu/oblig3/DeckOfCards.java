package ntnu.idatt2001.henriabu.oblig3;


import java.util.ArrayList;
import java.util.Random;

/**
 * Class representing a full deck of cards
 */
public class DeckOfCards {
    private final char[] suit = {'S', 'H', 'D', 'C'};
    private ArrayList<PlayingCard> cards = new ArrayList<>();
    private Random random = new Random();

    /**
     * Fills an Arraylist with objects representing av full deck of cards with the class PlayingCard
     * with suits Spades, Hearts, Diamonds an Cubes
     */

    public DeckOfCards(){
        for (int i = 0; i < suit.length; i++) {
            for (int j = 1; j < 14; j++) {
                cards.add(new PlayingCard(suit[i], j));
            }
        }
    }

    /**
     *
     * @return An arraylist of the 52 cards.
     */
    public ArrayList<PlayingCard> getCards(){
        return cards;
    }

    /**
     * Method which deals a poker hand. Puts random cards from the deck in a new Arraylist
     * with a size chosen by the user.
     * @param numberOfCards
     * @return An Arraylist of PlayingCards with a size between 1 and 7.
     */
    public ArrayList<PlayingCard> dealHand(int numberOfCards){
        if (numberOfCards<1 || numberOfCards>7){
            throw new IllegalArgumentException("Number must be between 1 and 7");
        }
        ArrayList<PlayingCard> pokerHand = new ArrayList<>();
        for (int i = 0; i < numberOfCards; i++) {
            int r = random.nextInt(cards.size());
            pokerHand.add(cards.get(r));
            cards.remove(r);
        }
        return pokerHand;
    }
}
