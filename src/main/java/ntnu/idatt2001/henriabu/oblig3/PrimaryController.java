package ntnu.idatt2001.henriabu.oblig3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class PrimaryController {

    @FXML private TextArea cardText = new TextArea();
    @FXML private TextField flushText = new TextField();
    @FXML private TextField queenOfSpadesText = new TextField();
    @FXML private TextField sumText = new TextField();
    @FXML private TextField numberOfCardsText = new TextField();
    @FXML Pane pane = new Pane();

    private DeckOfCards deckOfCards;
    private PokerHand pokerHand;
    private boolean firstHandDealt = false;

    @FXML Image grinImage = new Image(new FileInputStream("SpilleKortBilder/Grin.png"));
    @FXML ImageView v1 = new ImageView();
    @FXML ImageView v2 = new ImageView();
    @FXML ImageView v3 = new ImageView();
    @FXML ImageView v4 = new ImageView();
    @FXML ImageView v5 = new ImageView();
    @FXML ImageView v6 = new ImageView();
    @FXML ImageView v7 = new ImageView();
    private ArrayList<ImageView> imageViews = new ArrayList<>();

    private int numberOfCards;

    public PrimaryController() throws FileNotFoundException {
    }

    public void initialize(){
        imageViews.add(v4);
        imageViews.add(v5);
        imageViews.add(v3);
        imageViews.add(v6);
        imageViews.add(v2);
        imageViews.add(v7);
        imageViews.add(v1);
    }

    /**
     * Creates new objects of deckOfCards and PokerHand (using that deckOfCards).
     * Also resets the textfields and imageViews displaying the results from previous hands.
     * If the user gives invalid inputs exceptions are thrown.
     * Uses method displayCards to display the images of the cards
     * @throws FileNotFoundException
     */
    public void showHand() throws FileNotFoundException {
        try {
            for(ImageView i: imageViews){
                i.setImage(null);
            }
            numberOfCards = Integer.parseInt(numberOfCardsText.getText());
            firstHandDealt = true;
            cardText.setText("");
            flushText.setText("");
            queenOfSpadesText.setText("");
            sumText.setText("");
            deckOfCards = new DeckOfCards();
            pokerHand = new PokerHand(numberOfCards, deckOfCards);
            displayCards();
            System.out.println(pokerHand.getCardHand());

        }
        catch (NumberFormatException e){
            cardText.setText("Fill in a number between 1 and 7");
            v4.setImage(grinImage);
        }
        catch (IllegalArgumentException e){
            cardText.setText("Number must be between 1 and 7");
            v4.setImage(grinImage);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     *Checks the hand for different combinations.
     * If the method is called before a hand is dealt, the user is told to first deal a hand.
     */
    public void checkHand() {
        if (firstHandDealt) {
            if (pokerHand.checkFlush()) {
                flushText.setText("Yes!");
            } else {
                flushText.setText("No.");
            }
            if (pokerHand.checkForQueenOfSpades()) {
                queenOfSpadesText.setText("Yes!");
            } else {
                queenOfSpadesText.setText("No.");
            }
            sumText.setText(pokerHand.calcutlateSumOfFaces() + "");
        }
        else{
            cardText.setText("You must first deal a hand!");
        }
    }

    /**
     * Show only the hearts on hand.
     */
    public void showHeartsOnly(){
        if (firstHandDealt){
        cardText.setText(pokerHand.onlyHearts());}

        else {
            cardText.setText("You must first deal a hand!");
        }
    }

    /**
     * Find the image with the corresponding name to the cards on the hand and adds to the imageviews
     * in the Arraylist imageViews
     * @throws FileNotFoundException
     */
    public void displayCards() throws FileNotFoundException {
        for (int i = 0; i < numberOfCards; i++) {
            imageViews.get(i).setImage(new Image(new FileInputStream("SpilleKortBilder/" +
                    pokerHand.getCardHand().get(i).getAsString() + ".png")));
        }
    }

}
