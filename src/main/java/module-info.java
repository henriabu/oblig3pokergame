module ntnu.idatt2001.henriabu.oblig3 {
    requires javafx.controls;
    requires javafx.fxml;

    opens ntnu.idatt2001.henriabu.oblig3 to javafx.fxml;
    exports ntnu.idatt2001.henriabu.oblig3;
}